# Parent project does not export its library target, so this CML implicitly
# depends on being added from it, i.e. the testing is done only from the build
# tree and is not feasible from an install location

project(pbr_sandboxTests LANGUAGES CXX)

# ---- Dependencies ----

find_package(Catch2 REQUIRED)
include(Catch)

# ---- Tests ----

add_executable(pbr_sandbox_test source/pbr_sandbox_test.cpp)
target_link_libraries(
    pbr_sandbox_test PRIVATE
    pbr_sandbox_lib
    Catch2::Catch2WithMain
)
target_compile_features(pbr_sandbox_test PRIVATE cxx_std_17)

catch_discover_tests(pbr_sandbox_test)

# ---- End-of-file commands ----

add_folders(Test)

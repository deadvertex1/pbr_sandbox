#include <catch2/catch.hpp>

#include "lib.hpp"

TEST_CASE("Name is pbr_sandbox", "[library]")
{
  auto const lib = library {};
  REQUIRE(lib.name == "pbr_sandbox");
}

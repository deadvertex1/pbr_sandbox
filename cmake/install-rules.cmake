include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# find_package(<package>) call for consumers to find this project
set(package pbr_sandbox)

install(
    TARGETS pbr_sandbox_exe
    RUNTIME COMPONENT pbr_sandbox_Runtime
)

write_basic_package_version_file(
    "${package}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

# Allow package maintainers to freely override the path for the configs
set(
    pbr_sandbox_INSTALL_CMAKEDIR "${CMAKE_INSTALL_DATADIR}/${package}"
    CACHE PATH "CMake package config location relative to the install prefix"
)
mark_as_advanced(pbr_sandbox_INSTALL_CMAKEDIR)

install(
    FILES "${PROJECT_BINARY_DIR}/${package}ConfigVersion.cmake"
    DESTINATION "${pbr_sandbox_INSTALL_CMAKEDIR}"
    COMPONENT pbr_sandbox_Development
)

# Export variables for the install script to use
install(CODE "
set(pbr_sandbox_NAME [[$<TARGET_FILE_NAME:pbr_sandbox_exe>]])
set(pbr_sandbox_INSTALL_CMAKEDIR [[${pbr_sandbox_INSTALL_CMAKEDIR}]])
set(CMAKE_INSTALL_BINDIR [[${CMAKE_INSTALL_BINDIR}]])
" COMPONENT pbr_sandbox_Development)

install(
    SCRIPT cmake/install-script.cmake
    COMPONENT pbr_sandbox_Development
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()

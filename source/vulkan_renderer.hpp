#include <vulkan/vulkan.h>
#include <vulkan/vulkan.hpp>

class VulkanRenderer
{
public:
    struct ApiVersion
    {
        ApiVersion(uint32_t version);

        const uint32_t major;
        const uint32_t minor;
        const uint32_t patch;

    };

public:
    VulkanRenderer();
    void dumpInfo();

    ApiVersion getApiVersion() const;    
    ApiVersion getDriverVersion() const;

private:
    void createInstance();
    vk::PhysicalDevice pickPhysicalDevice();

private:
    vk::Instance _instance;
    vk::PhysicalDevice _physicalDevice;
};
#include "vulkan_renderer.hpp"

#include <fmt/core.h>

VulkanRenderer::ApiVersion::ApiVersion(uint32_t version)
    : major(VK_API_VERSION_MAJOR(version)),
    minor(VK_API_VERSION_MINOR(version)),
    patch(VK_API_VERSION_PATCH(version))
{
}

VulkanRenderer::VulkanRenderer()
{
    createInstance();

    _physicalDevice = pickPhysicalDevice();
}

void VulkanRenderer::createInstance()
{
    // Create vulkan instance
    vk::ApplicationInfo appInfo;
    appInfo.pApplicationName = "PBR Sandbox";
    appInfo.pEngineName = "";
    appInfo.apiVersion = VK_API_VERSION_1_2;

    vk::InstanceCreateInfo instanceCreateInfo;
    instanceCreateInfo.pApplicationInfo = &appInfo;
    _instance = vk::createInstance(instanceCreateInfo);
}

vk::PhysicalDevice VulkanRenderer::pickPhysicalDevice()
{
    auto physicalDevices = _instance.enumeratePhysicalDevices();
    return physicalDevices[0];
}

VulkanRenderer::ApiVersion VulkanRenderer::getApiVersion() const
{
    auto deviceProperties = _physicalDevice.getProperties2();
    return ApiVersion{deviceProperties.properties.apiVersion};
}

VulkanRenderer::ApiVersion VulkanRenderer::getDriverVersion() const
{
    auto deviceProperties = _physicalDevice.getProperties2();
    return ApiVersion{deviceProperties.properties.driverVersion};
}

void VulkanRenderer::dumpInfo()
{
    auto deviceProperties = _physicalDevice.getProperties2().properties;
    auto deviceFeatures = _physicalDevice.getFeatures2();
    auto apiVersion = getApiVersion();
    auto driverVersion = getDriverVersion();

    auto deviceMemoryProperties = _physicalDevice.getMemoryProperties2();

    // TODO: Move this outside of the VulkanRenderer class
    fmt::print("Vulkan API Version: {}.{}.{}\n", apiVersion.major, apiVersion.minor, apiVersion.patch);
    fmt::print("Driver Version: {}.{}.{}\n", driverVersion.major, driverVersion.minor, driverVersion.patch);
    fmt::print("Device Name: {}\n", deviceProperties.deviceName);
    fmt::print("Device Type: {}\n", vk::to_string(deviceProperties.deviceType));
}

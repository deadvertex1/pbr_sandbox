#include <iostream>
#include <string>

#include <glfwpp/glfwpp.h>
#include <fmt/core.h>

#include "lib.hpp"
#include "vulkan_renderer.hpp"

class ViewerApplication
{
public:
  ViewerApplication(glfw::Window &&window);
  void run();

private:
  glfw::Window _window;
};

ViewerApplication::ViewerApplication(glfw::Window &&window)
:   _window(std::move(window))
{
}

void ViewerApplication::run()
{
    while (!_window.shouldClose())
    {
        glfw::pollEvents();
    }
}

auto main() -> int
{
    auto const lib = library{};
    auto const message = "Hello from " + lib.name + "sdfhkjsdhfjkhd " + "!";
    std::cout << message << '\n';

    auto version = glfw::getCompileTimeVersion();
    fmt::print("GLFW Version: {}.{}.{}\n", version.major, version.minor, version.revision);

    // Initialize GLFW library which is deinitialized when this scope is exited
    auto GLFW = glfw::init();
    glfw::Window window{1024, 768, "PBR Viewer"};

    ViewerApplication app{std::move(window)};

    auto renderer = VulkanRenderer{};
    renderer.dumpInfo();

    app.run();

    return 0;
}
